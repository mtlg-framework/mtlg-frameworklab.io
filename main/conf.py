project = 'mtlg-framework'
copyright = '2020, MTLG'
author = 'Thiemo Leonhardt, Matthias Ehlenz'

version = '1.0'
release = '1.0'

from recommonmark.parser import CommonMarkParser

source_parsers = {
  '.md': CommonMarkParser,
}

html_additional_pages = {
  'index': 'index.html',
}

# extensions = [
# ]

templates_path = ['_templates']

# source_suffix = ['.rst', '.md']
source_suffix = '.md'

# The master toctree document.
master_doc = 'wikis/home'
# master_doc = 'home'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

html_theme = 'grayscalewiki'
html_theme_path = ["."]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


# Output file base name for HTML help builder.
htmlhelp_basename = 'hellodoc'
