<<<<<<< HEAD
This repository is built and served by gitlab.com at https://mtlg-framework.gitlab.io . It is the MTLG-Framework's public webpage. The page consists of the main landing page (see the folder `./mainsite`), a jsdoc API documentation and the MTLG game demo.
=======
# Webpage of the MTLG-Framework

This repository is built and served by gitlab.com at https://mtlg-framework.gitlab.io . It is the MTLG-Framework's public webpage. The page consists of the main landing page (see the folder `./main/_templates/index.html`), a jsdoc API documentation and the MTLG game demo.
>>>>>>> 2-integrate-gitlab-wiki

## Develop

Run following lines in (git-)bash to build and serve the pages:

```sh
npm install

# generate api documentation
jsdoc -c conf_jsdoc.json -d public/api/

# build demo game
mkdir demo_games
cd demo_games
npm init -f
npm install mtlg-gameframe
mtlg demo_games
mtlg build
mv build ../public/
cd ..

# download+build wiki and homepage
git clone --depth 1 https://gitlab.com/mtlg-framework/mtlg-gameframe.wiki.git main/wikis
pip install --user sphinx recommonmark
sphinx-build main public

# serve page
npm run serve
```

Then open the served page at http://localhost:8080 to see your changes.

To upload your changes, simply push to the master branch. The website gitlab.com will automatically build the website by running the commands from `.gitlab-ci.yml` and serving the static content from `./public`. You can see the build status at https://gitlab.com/mtlg-framework/mtlg-framework.gitlab.io/pipelines .
<<<<<<< HEAD
=======

To get more information about the used html-css-js-template, see <https://startbootstrap.com/template-overviews/grayscale/>.

If you modify files in `./main/_templates`, you can find a tutorial about how to use templating functions on <http://jinja.pocoo.org/docs/2.10/templates/> . The variables provided by sphinx can be found on <https://github.com/sphinx-doc/sphinx/blob/master/sphinx/themes/basic/layout.html> and <https://www.sphinx-doc.org/en/master/templating.html> .
>>>>>>> 2-integrate-gitlab-wiki
